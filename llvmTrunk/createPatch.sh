#!/bin/bash
set -x
set -e

diff -rupN llvm-still/ llvm/ > "patch.patch"
