# helloLLVM   
##simple project about hacking into Clang/LLVM
-----------------------
### Front-end part
Souce code:
~~~~
int main() {
    int acc = 0;
    __builtin___srxacc(acc);
    __builtin___xmac(12345,6789);
    acc = __builtin___lrxacc();
    return 0;
}
~~~~
passed to the `clang -cc1 -triple "sparc-unknown-unknown" file.c -S -emit-llvm` results into IR representation:
~~~~
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %acc = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 0, i32* %acc, align 4
  %0 = load i32* %acc, align 4
  call void @llvm.sparc.srxacc(i32 %0)
  call void @llvm.sparc.xmac(i32 12345, i32 6789)
  %1 = call i32 @llvm.sparc.lrxacc()
  store i32 %1, i32* %acc, align 4
  ret i32 0
}
~~~~
-----------------------
### Back-end part
Call to `llc -view-sched-dags file.ll` results to the following DAG (scheduler output):

![Alt text](https://bytebucket.org/wf34/hellollvm/raw/ec56567684384a933763c1c5fbcebfcb7ec14622/dag.png)
-----------------------

#####Asm-code:
~~~~
	.text
	.file	"file22.ll"
	.globl	main
	.align	4
	.type	main,@function
main:                                   ! @main
! BB#0:                                 ! %entry
	add %sp, -104, %sp
	st %g0, [%sp+100]
	st %g0, [%sp+96]
	sethi 0, %o0
	srxacc %o0, %xacc
	sethi 6, %o1
	or %o1, 645, %o1
	sethi 12, %o2
	or %o2, 57, %o2
	xmac %o2, %o1, %xacc
	lrxacc %xacc, %o1
	st %o1, [%sp+96]
	retl
	add %sp, 104, %sp
.Ltmp0:
	.size	main, .Ltmp0-main


	.ident	"clang version 3.6.0 (trunk)"
~~~~