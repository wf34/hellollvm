#!/bin/bash
set -e
set -x

mkdir "source"
cd "source"
svn export http://llvm.org/svn/llvm-project/llvm/trunk@215973 llvm
cd llvm/tools
svn export http://llvm.org/svn/llvm-project/cfe/trunk@215973 clang
cd ..
patch -p1 < ../../llvmTrunk/patch.patch
